%undefine _hardened_build
#I think one of the bundled libraries needs to be static.
#Static libraries are fine as all of the libraries included are bundled.
%undefine _cmake_shared_libs
%define _gcc_lto_cflags -fno-lto

#JIT is only supported on x86_64 and aarch64:
%ifarch x86_64 aarch64
%global enablejit 1
%endif

Name:           primehack-ishiiruka
Version:        1.0.2
Release:        0.2%{?dist}
Summary:        An Ishiiruka Emulator fork for Metroid Prime Trilogy

Url:            https://github.com/shiiion/Ishiiruka
##The project is licensed under GPLv2+ with some notable exceptions
#Source/Core/Common/GL/GLExtensions/* is MIT
#Source/Core/Core/HW/Sram.h is zlib
#Source/Core/Common/GekkoDisassembler.* is BSD (2 clause)
##The following is BSD (3 clause):
#dolphin-5.0/Source/Core/Common/SDCardUtil.cpp
#dolphin-5.0/Source/Core/Common/BitField.h
#dolphin-5.0/Source/Core/Core/IPC_HLE/l2cap.h
#dolphin-5.0/Source/Core/Core/IPC_HLE/hci.h
#dolphin-5.0/Source/Core/VideoBackends/Software/Clipper.cpp
#dolphin-5.0/Source/Core/AudioCommon/aldlist.cpp
##Any code in Externals has a license break down in Externals/licenses.md
License:        GPLv2+ and BSD and MIT and zlib
Source0:        https://github.com/shiiion/Ishiiruka/archive/%{version}/%{name}-%{version}.tar.gz
Source1:        %{name}.appdata.xml

Patch0:         externals.patch
Patch1:         0001-Implemented-alternate-Lerp-function.patch
Patch2:         0002-Fix-building-on-Linux.patch
Patch3:         0003-Avoid-crashing-when-analytics-is-disabled-at-build-t.patch
Patch4:         0001-Core-DSP-Fix-improper-uses-of-offsetof.patch
Patch5:         0001-Fix-building-with-new-g.patch

##Bundled code ahoy
#The following isn't in Fedora yet:
Provides:       bundled(FreeSurround)
Provides:       bundled(imgui) = 1.70
Provides:       bundled(cpp-argparse)
#Is this technically bundled code? Adding this just in case:
#https://github.com/AdmiralCurtiss/rangeset
Provides:       bundled(rangeset)
#soundtouch cannot be unbundled easily, as it requires compile time changes:
Provides:       bundled(soundtouch) = 2.1.2
#dolphin uses tests not included in upstream gtest (possibly unbundle later):
Provides:       bundled(gtest) = 1.9.0
#This is hard to unbundle and is unmaintainable with little benefit:
Provides:       bundled(glslang)

BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  alsa-lib-devel
BuildRequires:  bluez-libs-devel
%ifarch x86_64
BuildRequires:  bochs-devel
%endif
BuildRequires:  cmake
BuildRequires:  cubeb-devel
BuildRequires:  enet-devel
BuildRequires:  gtk2-devel
BuildRequires:  hidapi-devel
BuildRequires:  libao-devel
BuildRequires:  libcurl-devel
BuildRequires:  libevdev-devel
BuildRequires:  libpng-devel
BuildRequires:  libSM-devel
BuildRequires:  libusb-devel
BuildRequires:  libXi-devel
BuildRequires:  libXinerama-devel
BuildRequires:  libXrandr-devel
BuildRequires:  libXxf86vm-devel
BuildRequires:  libzstd-devel
BuildRequires:  lzo-devel
BuildRequires:  mbedtls-devel
BuildRequires:  mesa-libGL-devel
BuildRequires:  minizip-devel
BuildRequires:  miniupnpc-devel
BuildRequires:  openal-soft-devel
BuildRequires:  picojson-devel
BuildRequires:  pugixml-devel
BuildRequires:  pulseaudio-libs-devel
BuildRequires:  portaudio-devel
BuildRequires:  SDL2-devel
BuildRequires:  SFML-devel
BuildRequires:  SOIL-devel
BuildRequires:  spirv-headers-devel
BuildRequires:  spirv-tools
BuildRequires:  spirv-tools-devel
BuildRequires:  systemd-devel
BuildRequires:  vulkan-headers
BuildRequires:  xxhash-devel
BuildRequires:  zlib-devel
BuildRequires:  xz-devel

BuildRequires:  gettext
BuildRequires:  desktop-file-utils
BuildRequires:  libappstream-glib
BuildRequires:  hicolor-icon-theme
BuildRequires:  /usr/bin/env

#Only the following architectures are supported (64bit little endian only):
ExclusiveArch:  x86_64 aarch64 ppc64le

Requires:       hicolor-icon-theme
Requires:       %{name}-data = %{version}-%{release}

#Most of below is taken bundled spec file in source#
%description
Dolphin is a Gamecube, Wii and Triforce (the arcade machine based on the
Gamecube) emulator, which supports full HD video with several enhancements such
as compatibility with all PC controllers, turbo speed, networked multi player,
and more.
Most games run perfectly or with minor bugs.

%package data
Summary:        Dolphin Emulator data files
BuildArch:      noarch

%description data
This package provides the data files for primehack-ishiiruka.

####################################################

%prep
%autosetup -p1 -n Ishiiruka-%{version}

#Allow building with cmake macro
sed -i '/CMAKE_C.*_FLAGS/d' CMakeLists.txt

#Font license, drop the install directory into thie file
echo "%{_datadir}/%{name}/Sys/GC:" > font-licenses.txt
cat Data/Sys/GC/font-licenses.txt >> font-licenses.txt

#Fix for newer vulkan
sed -i "s/VK_PRESENT_MODE_RANGE_SIZE_KHR/(VkPresentModeKHR)("`
    `"VK_PRESENT_MODE_FIFO_RELAXED_KHR - VK_PRESENT_MODE_IMMEDIATE_KHR + 1)/" \
    Source/Core/VideoBackends/Vulkan/SwapChain.h

#Use primehack-ishiiruka as local datadir
sed -i '/^#define DOLPHIN_DATA_DIR "ishiiruka"$/s/ishiiruka/primehack-&/' \
    Source/Core/Common/CommonPaths.h

#This test fails without JIT enabled:
#https://bugs.dolphin-emu.org/issues/12421
%if ! 0%{?enablejit}
sed -i "/PageFaultTest/d" Source/UnitTests/Core/CMakeLists.txt
%endif

###Remove Bundled:
cd Externals
#Keep what we need...
rm -rf `ls | grep -v 'Bochs' | grep -v 'cpp-optparse' | grep -v 'soundtouch' | grep -v 'picojson' | grep -v 'gtest' | grep -v 'glslang' | grep -v 'wxWidgets3' | grep -v 'xbrz'`
#Remove Bundled Bochs source and replace with links (for x86 only):
%ifarch x86_64
pushd Bochs_disasm
rm -rf `ls | grep -v 'stdafx' | grep -v 'CMakeLists.txt'`
ln -s %{_includedir}/bochs/* ./
ln -s %{_includedir}/bochs/disasm/* ./
popd
#FIXME: This test fails because we unbundle bochs
sed -i "/x64EmitterTest/d" ../Source/UnitTests/Common/CMakeLists.txt
%else
rm -rf Bochs_disasm
%endif
#Replace bundled picojson with a modified system copy (remove use of throw)
pushd picojson
rm picojson.h
#In master, picojson has build option "PICOJSON_NOEXCEPT", but for now:
sed "s/throw std::.*;/std::abort();/g" /usr/include/picojson.h > picojson.h
popd

%build
#Script to find xxhash is not implemented, just tell cmake it was found
#Note some items are disabled to avoid bundling
#Set APPROVED_VENDORED_DEPENDENCIES to nothing to safe guard against bundling
%cmake . \
       -DAPPROVED_VENDORED_DEPENDENCIES=";" \
       -Ddatadir=%{_datadir}/%{name} \
       -DXXHASH_FOUND=ON \
       %{?!enablejit:-DENABLE_GENERIC=ON} \
       -DUSE_SHARED_ENET=ON \
       -DENABLE_ANALYTICS=OFF \
       -DENCODE_FRAMEDUMPS=OFF \
       -DUSE_DISCORD_PRESENCE=OFF
%cmake_build

%install
%cmake_install

rm -f %{buildroot}/%{_mandir}/man6/ishiiruka-nogui.6

# Rename stuff
mv %{buildroot}/%{_bindir}/ishiiruka %{buildroot}/%{_bindir}/%{name}
mv %{buildroot}/%{_datadir}/applications/ishiiruka.desktop %{buildroot}/%{_datadir}/applications/%{name}.desktop
find %{buildroot}/%{_mandir} %{buildroot}/%{_datadir}/locale %{buildroot}/%{_datadir}/icons/hicolor/*/apps -type f -name "ishiiruka*.*" | while read -r x; do
    dirname=$(dirname "$x")
    filename=$(basename "$x")
    dstfilename="primehack-$filename"
    mv "$x" "$dirname/$dstfilename"
done

sed -i -e 's/ishiiruka/primehack-&/' -e 's/Ishiiruka/PrimeHack &/' \
  %{buildroot}/%{_datadir}/applications/%{name}.desktop

#Install udev rules
mkdir -p %{buildroot}%{_udevrulesdir}
install -m 0644 Data/51-usb-device.rules %{buildroot}%{_udevrulesdir}/51-primehack-ishiiruka-usb-device.rules

#Create shell wrapper; dolphin doesn't work on wayland yet, but the QT GUI
#tries to use it. For now, force xwayland. Also fixes bodhi test warning
mv %{buildroot}/%{_bindir}/%{name} %{buildroot}/%{_bindir}/%{name}-x11
echo -e '#!/usr/bin/bash\nQT_QPA_PLATFORM=xcb %{name}-x11 "$@"' \
  > %{buildroot}/%{_bindir}/%{name}
#Remove workaround in desktop:
sed -i "s/^Exec=.*/Exec=primehack-ishiiruka/g" \
  %{buildroot}/%{_datadir}/applications/%{name}.desktop
#Symlink manpage
ln -s %{name}.6 %{buildroot}/%{_mandir}/man6/%{name}-x11.6

#Install appdata.xml
install -p -D -m 0644 %{SOURCE1} \
  %{buildroot}/%{_datadir}/appdata/%{name}.appdata.xml
%find_lang %{name}

%check
# FIXME
#%%cmake_build --target unittests
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}.desktop
appstream-util validate-relax --nonet \
  %{buildroot}/%{_datadir}/appdata/*.appdata.xml

%files -f %{name}.lang
%doc Readme.md
%license license.txt
%attr(755, root, root) %{_bindir}/%{name}
%{_bindir}/%{name}-x11
%{_mandir}/man6/%{name}.*
%{_mandir}/man6/%{name}-x11.*
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.*
%{_datadir}/%{name}/sys/Resources/
%{_datadir}/%{name}/sys/Themes/
%{_datadir}/appdata/*.appdata.xml

%files data
%doc Readme.md docs/gc-font-tool.cpp
%license license.txt font-licenses.txt
#For the gui package:
%exclude %{_datadir}/%{name}/sys/Resources/
%exclude %{_datadir}/%{name}/sys/Themes/
#Already packaged:
%exclude %{_datadir}/%{name}/sys/GC/font-licenses.txt
%{_datadir}/%{name}
%{_udevrulesdir}/51-primehack-ishiiruka-usb-device.rules

%changelog
* Fri Feb 26 2021 Timothy Redaelli <tredaelli@redhat.com> - 1.0.2-0.2
- Use primehack-ishiiruka as local datadir

* Thu Feb 25 2021 Timothy Redaelli <tredaelli@redhat.com> - 1.0.2-0.1
- Avoid crash when opening configuration

* Thu Feb 25 2021 Timothy Redaelli <tredaelli@redhat.com> - 1.0.2-0
- Initial package (adapted from Fedora dolphin-emu.spec)

